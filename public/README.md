![](/static/images/ayanami.jpg)

This car was built in September 1998.    
This car was manufactured in [Smyrna, Tennessee](https://en.wikipedia.org/wiki/Nissan_Smyrna_assembly_plant).    
This car's engine was manufactured in Aguascalientes, Mexico.    
This car had an estimated lifespan of twelve years.    
This car's engine had a reported real world lifespan of 160,000 to 180,000 miles.   

This car was sold in Iowa.   
This car was first sold by Aiby, the Lead Sales representative.   
This car had an additional owner.   
This car ended up in Kansas City, Kansas.   

This car was bought by a struggling student in 2017.   
This car took that struggling student back and forth from their first pharmacy job.   
This car took that student back and forth to school.   
This car took that student to their certification examination.   
This car welcomed that student back as a Nationally Certified Technician.   
This car helped that student move into their first ever apartment.   

This car took that student to their dream job interview.   
This car welcomed them back when they were extended an offer.   
This car took them to their last full day at their first job in pharmacy.   
This car took them to their first day working for a non-profit.   

This car took them to two different jobs, for a while.   
This car spent every lunch with them at the new job.   
This car was the first to find out the non-profit had been purchased by a for-profit.   

This car made sure group home client medications were delivered.   
This car didn't care how late it was.   
This car didn't care how cold it was.   
This car didn't care where the home was located.   
This car didn't care how long they had been out.   
This car always made sure the technician made it home.   

This car took the technician to their appointments.   
This car helped them meet with a lawyer.   
This car helped them change their name.   
This car helped them become female.   

This car was there when the technician and their fiancé adopted a rescue cat.   
This car was used to hide the rescue cat from the landlord inspection.   
This car was there when the cat celebrated his first birthday.   

This car comforted the technician as they cried on the way to their for-profit job.   
This car played whatever books they asked it to.   
This car comforted the technician as they left.   
This car watched the dream become a nightmare.   

This car heard the breakdown that lead to them looking for a new job.   
This car took them to an interview for a non-profit hospital system.   
This car comforted them as they waited to walk in.   
This car first heard the news of how well they thought they did.   

This car took them to the police station to get fingerprinted for their new license.   
This car took them to the last day of their former dream job.   
This car took them to the first day of their new job.   
This car saw them achieve their lifelong dream of working in a hospital.   

This car first heard about the panicked need for a wedding between the technician and their fiancé.   
This car was owned when the technician became a wife.   

This car was asked to play music too loudly.   
This car was there when the technician discovered new bands.   
This car, unfortunately, lost a few speakers.   

This car was asked to continue performing when it couldn't always be maintained.   
This car made sure the heat was always warm.   
This car made sure the air was always cold.   
This car made sure everyone got where they were going.   

This car was left dirty a bit too often.   
This car wasn't cleaned as much as it should have been.   
This car was neglected for maintenance tasks.   
This car continued to run.   

This car was there for the start of COVID-19.   
This car started struggling in 2020, much like everyone else.   
This car chewed through two alternators and a battery in one week.   
This car began leaking oil.   
This car leaked radiator fluid.   
This car leaked power steering fluid.   
This car gave the technician grey hairs.   

This car made sure the technician got to work during record breaking cold in 2021.   
This car made sure the technician made it home.   
This car was important enough that the technician stayed at work one night, rather than ask more of it.   
This car saw the cold spell break.   

This car overexerted itself during the cold snap.   
This car first told the technician there was a problem the week after.   
This car said it had had enough Monday, February 22, 2021.   

This car was asked to make one last drive so it could be picked up for scrapping.   
This car didn't have an engine hiccup once during the last drive.   
This car had electrical issues that caused the interior lamp to come on when the break was pressed.   
This car made sure the heat was still warm during a cold night.   

This car made it all the way home with no issues.   
This car began to shake when it was turned into it's last parking spot.   
This car stalled out for the last time when the technician was home safe.   
This car never left anyone stranded.   

This car broke down for the last time, February 27th, 2021.   
This car was 22 years old.    
This car exceeded its anticipated service life by a decade.   
This car had 190,000 miles.   
This car had an engine that exceeded its service life by 10-30k miles.   
This car will give away the parts of it that still function so that other cars may continue to function.   

This car's name was Ayanami.   
This car was the technician's friend.   
This car will be sorely missed.   
